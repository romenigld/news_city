require 'rails_helper'

RSpec.describe ReviewPolicy do
  subject { ReviewPolicy.new(user, review) }

  let(:user)   { create(:user) }
  let(:post)   { create(:post) }
  let(:review) { create(:review, post: post) }

  context "for anonymous users" do
    let(:user) { nil }
    it { should_not permit_action :create }
  end

  context "for viewers of the post" do
    before {assign_role!(user, :viewer, post) }
    it { should_not permit_action :create }
  end

  context "for editors of the post" do
    before {assign_role!(user, :editor, post) }
    it { should permit_action :create }
  end

  context "for manager of the post" do
    before {assign_role!(user, :manager, post) }
    it { should permit_action :create }
  end

  context "for managers of other posts" do
    before do
      assign_role!(user, :manager, create(:post))
    end
    it { should_not permit_action :create }
  end

  context "for administrators" do
    let(:user) { create(:user, :admin) }
    it { should permit_action :create }
  end
end
