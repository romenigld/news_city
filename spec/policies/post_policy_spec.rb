require 'rails_helper'

RSpec.describe PostPolicy do

  context "permissions" do
    subject { PostPolicy.new(user, post) }

    let(:user) { create :user }
    let!(:post) { create(:post, author: user) }
    let(:category) { create(:category)}

    context "for anonymous users" do
      let(:user) { nil }

      it { should_not permit_action :show }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
      it { should_not permit_action :publish }
      it { should_not permit_action :unpublish }
      it { should_not permit_action :change_state }
      it { should_not permit_action :change_category }
      it { should_not permit_action :tag }
    end

    context "for viewers of the post" do
      before { assign_role!(user, :viewer, post) }

      it { should permit_action :show }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
      it { should_not permit_action :publish }
      it { should_not permit_action :unpublish }
      it { should_not permit_action :change_state }
      it { should_not permit_action :change_category }
      it { should_not permit_action :tag }
    # It can also be written like the following.
    # it { is_expected.to permit_action :show }
    # it { is_expected.to_not permit_action :update }
    end

    context "for editors of the post" do
      before { assign_role!(user, :editor, post) }

      it { should permit_action :show }
      it { should permit_action :update }
      it { should_not permit_action :destroy }
      it { should_not permit_action :publish }
      it { should_not permit_action :unpublish }
      it { should_not permit_action :change_state }
      it { should_not permit_action :change_category }
      it { should_not permit_action :tag }
    end

    context "for editors of other posts" do
      before do
        assign_role!(user, :editor, create(:post))
      end

      it { should_not permit_action :show }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
      it { should_not permit_action :publish }
      it { should_not permit_action :unpublish }
      it { should_not permit_action :change_state }
      it { should_not permit_action :change_category }
      it { should_not permit_action :tag }
    end

    context "for managers of the post" do
      before { assign_role!(user, :manager, post) }

      it { should permit_action :show }
      it { should permit_action :update }
      it { should_not permit_action :destroy }
      it { should permit_action :publish }
      it { should permit_action :unpublish }
      it { should permit_action :change_state }
      it { should permit_action :change_category }
      it { should permit_action :tag }
    end

    context "for managers of other posts" do
      before do
        assign_role!(user, :manager, create(:post))
      end

      it { should_not permit_action :show }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
      it { should_not permit_action :publish }
      it { should_not permit_action :unpublish }
      it { should_not permit_action :change_state }
      it { should_not permit_action :change_category }
      it { should_not permit_action :tag }
    end

    context "for administrators" do
      let(:user) { create(:user, :admin) }

      it { should permit_action :show }
      it { should permit_action :update }
      it { should permit_action :destroy }
      it { should permit_action :publish }
      it { should permit_action :unpublish }
      it { should permit_action :change_state }
      it { should permit_action :change_category }
      it { should permit_action :tag }
    end
  end
end
