require "rails_helper"

RSpec.feature "Users can review on posts" do
  let(:user) { create(:user) }
  let(:post) { create(:post, author: user) }

  before do
    login_as(user)
    assign_role!(user, :manager, post)
  end

  scenario "with valid attributes" do
    visit post_path(post)
    fill_in "review_text", with: "Added a review!"
    click_button "Create Review"

    expect(page).to have_content "Review has been created."
    within("#reviews") do
      expect(page).to have_content "Added a review!"
    end
  end

  scenario "with invalid attributes" do
    visit post_path(post)
    click_button "Create Review"

    expect(page).to have_content "Review has not been created."
  end

  scenario "when changing a post's state" do
    create(:state, name: "Open")

    visit post_path(post)
    fill_in "review_text", with: "This is a real issue"
    select "Open", from: "State"
    click_button "Create Review"

    expect(page).to have_content "Review has been created."

    within("#reviews") do
      expect(page).to have_content "state changed to Open"
    end
  end

  scenario "but cannot charge the state without permission" do
    assign_role!(user, :editor, post)
    visit post_path(post)

    expect(page).not_to have_select "State"
  end

  scenario "when adding a new tag to a post" do
    visit post_path(post)
    expect(page).not_to have_content "bug"

    fill_in "review_text", with: "Adding the bug tag"
    fill_in "Tags", with: "bug"
    click_button "Create Review"

    expect(page).to have_content "Review has been created."
    within("#post #tags") do
      expect(page).to have_content "bug"
    end
  end
end
