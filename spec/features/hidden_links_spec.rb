require "rails_helper"

RSpec.feature "Users can only see the appropriate links" do
  let(:user) { create(:user) }
  let(:admin) { create(:user, :admin) }
  let(:post) { create(:post, author: user) }

  context "anonymous users" do
    scenario "cannot see the New Post link" do
      visit "/"
      expect(page).not_to have_link "New Post"
    end

    scenario "cannot show the Post" do
      visit post_path(post)
      expect(page).to have_content "You aren't allowed to do that."
    end

    scenario "cannot show the watch or unwatch link" do
      visit post_path(post)
      expect(page).not_to have_button "Watch"
      expect(page).not_to have_button "Unwatch"
    end
  end

  context "regular users or non-admin users(post viewers)" do
    before do
      login_as(user)
      assign_role!(user, :viewer, post)
    end

    scenario "cannot see the New Post link" do
      visit "/"
      expect(page).not_to have_link "New Post"
    end

    scenario "cannot see the Edit Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Edit Post"
    end

    scenario "cannot see the Delete Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Delete Post"
    end

    scenario "cannot see the Publish Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Publish"
    end

    scenario "cannot see the Unpublish Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Unpublish"
    end

    scenario "cannot see the New Review form" do
      visit post_path(post)
      expect(page).not_to have_heading "New Review"
    end

    scenario "cannot show the watch or unwatch link" do
      visit post_path(post)

      expect(page).not_to have_button "Watch"
      expect(page).not_to have_button "Unwatch"
    end
  end

  context "Editor users" do
    before do
      login_as(user)
      assign_role!(user, :editor, post)
    end

    scenario "cannot see the New Post link" do
      visit "/"
      expect(page).not_to have_link "New Post"
    end

    scenario "can see the Edit Post link" do
      visit post_path(post)
      expect(page).to have_link "Edit Post"
    end

    scenario "cannot see the Delete Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Delete Post"
    end

    scenario "cannot see the Publish Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Publish"
    end

    scenario "cannot see the Unpublish Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Unpublish"
    end

    scenario "can see the New Review form" do
      visit post_path(post)
      expect(page).to have_heading "New Review"
    end
  end

  context "Manager users" do
    before do
      login_as(user)
      assign_role!(user, :manager, post)
    end

    scenario "cannot see the New Post link" do
      visit "/"
      expect(page).not_to have_link "New Post"
    end

    scenario "can see the Edit Post link" do
      visit post_path(post)
      expect(page).to have_link "Edit Post"
    end

    scenario "cannot see the Delete Post link" do
      visit post_path(post)
      expect(page).not_to have_link "Delete Post"
    end

    scenario "can see the Publish Post link" do
      visit post_path(post)
      expect(page).to have_link "Publish"
    end

    scenario "can see the Unpublish Post link" do
      visit post_path(post)
      click_link "Publish"
      expect(page).to have_link "Unpublish"
    end

    scenario "can see the New Review form" do
      visit post_path(post)
      expect(page).to have_heading "New Review"
    end
  end

  context "Admin users" do
    before do
      login_as(admin)
      create(:post, author: admin)
    end

    scenario "can see the New Post link" do
      visit "/"
      expect(page).to have_link "New Post"
    end

    scenario "can see the Edit Post link" do
      visit post_path(post)
      expect(page).to have_link "Edit Post"
    end

    scenario "can see the Delete Post link" do
      visit post_path(post)
      expect(page).to have_link "Delete Post"
    end

    scenario "can see the Publish Post link" do
      visit post_path(post)
      expect(page).to have_link "Publish"
    end

    scenario "can see the Unpublish Post link" do
      visit post_path(post)
      click_link "Publish"
      expect(page).to have_link "Unpublish"
    end

    scenario "can see the New Review form" do
      visit post_path(post)
      expect(page).to have_heading "New Review"
    end
  end
end
