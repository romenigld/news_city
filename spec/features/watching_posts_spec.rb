require "rails_helper"

RSpec.feature "Users can watch and unwatch posts"  do
  let(:user) { create(:user) }
  let(:post) do
    create(:post , author: user)
  end

  before do
    assign_role!(user, "viewer", post)
    login_as(user)
    visit post_path(post)
  end

  scenario "successfully" do
    within("#watchers") do
      expect(page).to have_content user.email
    end

    click_link "Unwatch"
    expect(page).to have_content "You are no longer watching this " + "post."

    within("#watchers") do
      expect(page).to_not have_content user.email
    end

    click_link "Watch"
    expect(page).to have_content "You are now watching this post."

    within("#watchers") do
      expect(page).to have_content user.email
    end
  end
end
