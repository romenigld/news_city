require "rails_helper"

RSpec.feature "Users can search for posts matching specific criteria" do
  let(:user) { create(:user) }
  let!(:post_1) do
    create(:post, title: "Create posts and search by tags",
                  content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                  tag_names: "iteration_1",
                  author: user)
  end

  let!(:post_2) do
    create(:post, title: "Create users and restrict them with pundit",
                  content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                  tag_names: "iteration_2",
                  author: user)
  end

  before do
    assign_role!(user, :manager, post_1)
    assign_role!(user, :manager, post_2)
    login_as(user)
    visit posts_path
  end

  scenario "searching by title" do
    fill_in "Search", with: "posts"
    click_button "Search"
    within("#posts") do
      expect(page).to have_link "Create posts and search by tags"
      expect(page).to_not have_link "Create users and restrict them with pundit"
    end
  end
end
