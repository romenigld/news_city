require 'rails_helper'

RSpec.feature "Posts Managers can" do
  let(:user) { create(:user) }
  let(:post) { create(:post, title: "Publishing a post", author: user ) }
  let(:category) { create(:category) }

  before do
    post.categories << category
    login_as(user)
    assign_role!(user, :manager, post)
    visit "/"
    click_link "Publishing a post"
    click_link "Publish"
  end

  scenario "publish posts" do
    expect(page).to have_content "Post was successfully published."
    expect(page).to_not have_link "Publish"

    post = Post.find_by(title: "Publishing a post")
    expect(page.current_url).to eq post_url(post)

    within("#post #attributes tr.published") do
      expect(page).to have_content "Published: #{post.published}"
    end

  end

  scenario "unpublish posts" do
    click_link "Unpublish"

    expect(page).to have_content "Post was successfully unpublished."
    expect(page).to_not have_link "Unpublish"

    post = Post.find_by(title: "Publishing a post")
    expect(page.current_url).to eq post_url(post)
    expect(post.published_at).to eq nil

    within("#post #attributes tr.published") do
      expect(page).to have_content "Published: not yet!"
    end
  end
end
