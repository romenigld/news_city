require "rails_helper"

RSpec.feature "Users can view posts by category" do
  let(:user) {  create(:user) }
  let(:post) { create(:post, title: "Surf at Zurriola beach", author: user) }
  let(:category) { create(:category)}

  before do
    post.categories << category
    login_as(user)
    assign_role!(user, :viewer, post)
  end

  scenario "Sports category" do
    visit '/'
    click_link 'Sports'
    expect(page.current_url).to eq category_url(category)

    title = "Category - Sports - News City"
    expect(page).to have_title title
    expect(page).to_not have_content "Surf at Zurriola beach"

    post.publish

    click_link 'Sports'
    expect(page).to have_content "Surf at Zurriola beach"
  end
end
