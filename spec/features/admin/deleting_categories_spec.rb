require 'rails_helper'

RSpec.feature "Admins can delete categories" do
  before do
    login_as(create(:user, :admin))
  end

  scenario "succesfully" do
    create(:category)
    visit admin_root_path

    within("#admin .categories-admin") do
      click_link 'List all by order'
    end

    within(".admin-category") do
      click_link "Sports"
    end
    click_link "Delete Category"

    expect(page).to have_content "Category has been deleted."
    expect(page.current_url).to eq admin_categories_url
    expect(page).to have_no_content "Sports"
  end
end
