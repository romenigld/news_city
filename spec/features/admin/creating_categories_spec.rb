require 'rails_helper'

RSpec.feature "Admins can create categories" do
  let(:admin) { create(:user, :admin) }

  before do
    login_as(admin)
    visit admin_root_path
    within("#admin .categories-admin") do
      click_link "List all by order"
    end
    #expect(page.current_url).to eq admin_categories_path
    click_link "New Category"
  end

  scenario "with valid attributes" do
    fill_in "Name", with: "Sports"

    click_button "Create Category"

    expect(page).to have_content "Category has been created."

    category = Category.find_by(name: "Sports")
    expect(page.current_url).to eq admin_category_url(category)

    title = "Category - #{category.name} - News City"
    expect(page).to have_title title
  end

  context "when providing invalid attributes" do
    scenario "name can't be blank" do
      click_button "Create Category"

      expect(page).to have_content "Category has not been created."
      expect(page).to have_content "Name can't be blank"
    end

    scenario "name can't be less 3 characters" do
      words = "c" * 2
      expect{ create(:category, name: words) }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name is too short (minimum is 3 characters)")
    end

    scenario "name can't be more than 30 character" do
      words = "c" * 31
      expect{ create(:category, name: words) }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name is too long (maximum is 30 characters)")
    end

    scenario "names can't be equal" do
      create(:category, name: "Name equal")

      expect{ create(:category, name: "Name equal") }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name has already been taken")
    end
  end
end
