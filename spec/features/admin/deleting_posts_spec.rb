require "rails_helper"

RSpec.feature "Admins can delete posts" do

  let(:author) { create(:user, :admin) }
  let!(:post) { create(:post, title: "Sublime Text 3", author: author) }

  before do
    login_as(author)
  end
  scenario "successfully" do
    visit "/"
    click_link "Sublime Text 3"
    click_link "Delete Post"

    expect(page).to have_content "Post has been deleted."
    expect(page.current_url).to eq posts_url
    expect(page).to have_no_content "Sublime Text 3"
  end

end
