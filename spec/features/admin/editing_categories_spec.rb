require 'rails_helper'

RSpec.feature "Admins can edit existing categories" do
  let(:admin_user) { create(:user, :admin) }
  let(:category) { create(:category) }

  before do
    login_as(admin_user)
    visit admin_category_path(category)
    click_link "Edit Category"
  end

  context "with valid attributes" do
    scenario "name can't be blank" do
      title = "Edit Category - Sports - News City"
      expect(page).to have_title title

      fill_in "Name", with: "Gastronomy"
      click_button "Update Category"

      expect(page).to have_content "Category has been updated."
      expect(page).to have_content "Gastronomy"
      expect(page).to_not have_content "Sports"
    end
  end
end
