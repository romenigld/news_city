require 'rails_helper'

RSpec.feature "Admins can create new posts" do
  let!(:state)   { create(:state, name: "New", default: true) }
  let(:admin)    { create(:user, :admin) }
  let(:category) { create(:category) }

  before do
    @categories = category
    login_as(admin)
    visit "/"
    click_link "New Post"
  end

  scenario "with valid attributes" do
    fill_in "Title", with: "Sublime Text 3"
    fill_in "Subtitle", with: "A text editor for everyone"
    fill_in "Content", with: "It's a text editor who you can use for free in trial version,
                              but it will complaining a text message for update paying the editor."
    click_button "Create Post"

    expect(page).to have_content "Post has been created."
    #expect(page).to have_content "State:New"

    within("#post #attributes tr.estado th") do
      expect(page).to have_content "State:"
    end

    within("#post #attributes tr.estado td") do
      expect(page).to have_content "New"
    end

    post = Post.find_by(title: "Sublime Text 3")
    expect(page.current_url).to eq post_url(post)

    title = "Post - Sublime Text 3 - News City"
    expect(page).to have_title title

    within("#post #attributes tr.author") do
      expect(page).to have_content "Author: #{post.author.email}"
    end

    within("#post #attributes tr.created_at") do
      expect(page).to have_content "Created: #{time_ago_in_words(post.created_at)} ago"
    end

    within("#post #attributes tr.published") do
      expect(page).to have_content "Published: #{post.published}"
    end
  end

  context "when providing invalid attributes" do
    scenario "title can't be less then 10 characters" do
      words = "a" * 9
      expect{ create(:post, title: words) }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Title is too short (minimum is 10 characters)")
    end

    scenario "title can't be more then 100 characters" do
      words = "a" * 101
      expect{ create(:post, title: words) }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Title is too long (maximum is 100 characters)")
    end

    scenario "title can't be blank" do
      click_button  "Create Post"

      expect(page).to have_content "Post has not been created."
      expect(page).to have_content "Title can't be blank"
    end

    scenario "titles can't be the same" do
        create(:post, title: "The titles can't be the same")

        expect{ create(:post, title: "The titles can't be the same") }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Title has already been taken")
    end

    scenario "subtitle can't be more then 100 characters" do
      words = "a" * 101
      expect{ create(:post, subtitle: words) }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Subtitle is too long (maximum is 100 characters)")
    end

    scenario "content can't be less then 30 characters" do
      words = "a" * 29
      expect{ create(:post, content: words) }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Content is too short (minimum is 30 characters)")
    end

    scenario "content can't be blank" do
      click_button  "Create Post"

      expect(page).to have_content "Post has not been created."
      expect(page).to have_content "Content can't be blank"
    end
  end

  context "upload of a image" do
    scenario "with an attachment of image" do
      fill_in "Title", with: "Add image for posts"
      fill_in "Subtitle", with: "An image for the posts "
      attach_file "File", "#{Rails.root}/spec/fixtures/attachment.jpeg"
      fill_in "Content", with: "It will serve for show images about the posts."
      click_button "Create Post"

      expect(page).to have_content "Post has been created."

      within("#post .text-center .attachment") do
        expect(page).to have_css("img[src*='attachment.jpeg']")
      end
    end

    scenario "persisting file uploads across form displays" do
      attach_file "File", "#{Rails.root}/spec/fixtures/attachment2.jpg"
      click_button "Create Post"

      expect(page).to have_content "Post has not been created."

      within(".attachment") do
        expect(page).to have_css("img[src*='attachment2.jpg']")
      end

      fill_in "Title", with: "Add image for posts with thumb"
      fill_in "Subtitle", with: "An image for the posts "
      fill_in "Content", with: "It will serve for show images about the posts."
      click_button "Create Post"

      within("#post .text-center .attachment") do
        expect(page).to have_css("img[src*='attachment2.jpg']")
      end
    end

    scenario "upload a image from a remote location", slow: true do
      fill_in "Title", with: "Add image for posts with a remote location"
      fill_in "Subtitle", with: "An image for the posts with a remote location "
      fill_in "post_remote_attachment_url", with: "http://lorempixel.com/output/city-q-c-640-480-4.jpg"
      fill_in "Content", with: "It will serve for show images about the posts. With a remote location. like a link of an image."
      click_button "Create Post"

      expect(page).to have_content "Post has been created."

      within("#post .attachment") do
        expect(page).to have_css("img[src*='thumb_city-q-c-640-480-4.jpg']")
      end
    end

    context "when providing invalid attributes" do
      scenario "recuse when providing an image that exceeds 1 megabytes" do
        fill_in "Title", with: "Add image for posts"
        fill_in "Subtitle", with: "An image for the posts "
        attach_file "File", "#{Rails.root}/spec/fixtures/Ant_Nebula.jpg"
        fill_in "Content", with: "It will serve for show images about the posts."
        click_button "Create Post"

        expect(page).to have_content "Post has not been created."
        expect(page).to have_content "file size must be less than 1 MB"
      end

      scenario "recuse when upload files that aren't image files" do
        fill_in "Title", with: "Add image for posts"
        fill_in "Subtitle", with: "An image for the posts "
        attach_file "File", "#{Rails.root}/spec/fixtures/file.txt"
        fill_in "Content", with: "It will serve for show images about the posts."
        click_button "Create Post"

        expect(page).to have_content "Post has not been created."
        expect(page).to have_content('You are not allowed to upload "txt" files, allowed types: jpg, jpeg, gif, png')
      end
    end
  end

  context "with categories" do
    scenario  "when select the categories with checkboxes" do
      check 'Sports'
      fill_in "Title", with: "An Restaurant"
      fill_in "Subtitle", with: "Good and healthy for everyone"
      fill_in "Content", with: "It's a text editor who you can use for free in trial version,
                                but it will complaining a text message for update paying the editor."

      click_button "Create Post"

      expect(page).to have_content "Post has been created."

      within("#post .category") do
        expect(page).to have_content "Sports"
      end
    end
  end

  scenario "with associated tags" do
    fill_in "Title", with: "Non-standards compliance"
    fill_in "Subtitle", with: "Post have tags"
    fill_in "Content", with: "Posts now have different tags!!"
    fill_in "Tags", with: "browser visual"
    click_button "Create Post"

    expect(page).to have_content "Post has been created."

    within("#post #tags") do
      expect(page).to have_content "browser"
      expect(page).to have_content "visual"
    end
  end
end
