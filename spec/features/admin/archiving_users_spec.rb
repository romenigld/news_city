require "rails_helper"

RSpec.feature "Admins can" do
  let(:admin_user) { create(:user, :admin) }
  let(:user) { create(:user) }
  let(:post) { create(:post, author: user) }

  before do
    assign_role!(user, :editor, post)
    login_as(admin_user)
    visit admin_user_path(user)
    click_link "Archive User"
  end

  scenario "archive users successfully" do
    expect(page).to have_content "User has been archived."
    expect(page.current_url).to eq archived_admin_users_url
    expect(page).to have_content user.email
    expect(page).not_to have_link "Unarchive User"
  end

  scenario "unarchive users successfully" do
    visit admin_user_path(user)
    click_link "Unarchive User"
    expect(page).to have_content "User has been unarchived."
    expect(page.current_url).to eq admin_users_url
    expect(page).to have_content user.email
    expect(page).not_to have_link "Archive User"
  end

  scenario "but cannot archive themselves" do
    visit admin_user_path(admin_user)
    click_link "Archive User"
    expect(page).to have_content "You cannot archive yourself!"
  end

  context "nullify" do
    before do
      visit admin_users_path
      click_link "Archived users"
      click_link user
    end

    scenario "roles posts of the user archived" do
      click_link "Nullify Roles"
      expect(page).to have_content "The user #{user} has been nullified the roles in posts who was member."
    end

    scenario "author posts of the user archived" do
      click_link "Nullify Posts"
      expect(page).to have_content "The user #{user} has been nullified the posts as author."
    end
  end

  context "The links are not showed for nullify" do
    before do
      visit admin_user_path(user)
      click_link "Unarchive User"
    end

    scenario "roles posts of the user when the user aren't archived" do
      expect(page).not_to have_link "Nullify Roles"
      # click_link "Nullify Roles"
      # expect(page).to have_content "Can't be nullify the posts of the user #{user}, because it isn't an archived user."
    end

    scenario "author posts of the use when the user aren't archived" do
      expect(page).not_to have_link "Nullify Posts"
      # click_link "Nullify Posts"
      # expect(page).to have_content "Can't be nullify the posts of the user #{user}, because it isn't an archived user."
    end
  end
end
