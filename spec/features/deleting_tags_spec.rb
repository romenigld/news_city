require "rails_helper"

RSpec.feature "Users cand elete unwanted tags from a post" do
  let(:user) { create(:user) }
  let(:post) { create(:post, tag_names: "ThisTagMustDie", author: user) }

  before do
    login_as(user)
    assign_role!(user, :manager, post)
    visit post_path(post)
  end

  scenario "successully", js: true do
    within tag("ThisTagMustDie") do
      click_link "remove"
    end

    expect(page).to_not have_content "ThisTagMustDie"
  end
end
