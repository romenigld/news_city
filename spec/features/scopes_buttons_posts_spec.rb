require "rails_helper"

RSpec.feature "Admins can filter posts by" do
  let(:admin) {  create(:user, :admin) }
  let(:post) { create(:post, title: "This post is unpublished", author: admin) }
  let(:post_with_publish_date) { create(:post, :with_publish_date, title: ":with_publish_date, Scopes of the Posts", author: admin) }
  #let(:post_in_the_future) { create(:post, :in_the_future, title: ":in_the_future, Scopes of the Posts", author: user ) }
  let(:post_in_the_past ) { create(:post, :in_the_past , title: ":in_the_past, Scopes of the Posts", author: admin) }
  let(:post_one_month_ago ) { create(:post, :one_month_ago , title: ":one_month_ago, Scopes of the Posts", author: admin) }

  before do
    login_as(admin)
    assign_role!(admin, :editor, post)
    assign_role!(admin, :editor, post_with_publish_date)
    assign_role!(admin, :editor, post_in_the_past)
    assign_role!(admin, :editor, post_one_month_ago)
  end

  scenario "published" do
    visit "/"
    click_link "Published"
    expect(page).to have_content ":with_publish_date, Scopes of the Posts"
    expect(page).to have_content ":in_the_past, Scopes of the Posts"
    expect(page).to have_content ":one_month_ago, Scopes of the Posts"
    expect(page).to_not have_content "This post is unpublished"
  end

  scenario "draft" do
    visit "/"
    click_link "Draft"
    expect(page).to_not have_content ":with_publish_date, Scopes of the Posts"
    expect(page).to_not have_content ":in_the_past, Scopes of the Posts"
    expect(page).to_not have_content ":one_month_ago, Scopes of the Posts"
    expect(page).to have_content "This post is unpublished"
  end

  scenario "recent" do
    visit "/"
    click_link "Recent"
    expect(page).to have_content ":with_publish_date, Scopes of the Posts"
    expect(page).to have_content ":in_the_past, Scopes of the Posts"
    expect(page).to_not have_content "This post is unpublished"
    expect(page).to_not have_content ":one_month_ago, Scopes of the Posts"
  end
end
