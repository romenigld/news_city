require "rails_helper"

RSpec.feature "Posts managers can edit existing posts" do
  let(:user) { create(:user) }
  let(:post) { create(:post,
                      title: "Sublime Text 3",
                      subtitle: "A text editor for everyone",
                      content: "It's a text editor who you can use for free in trial version,
                                but it will complaining a text message for update paying the editor.",
                      author: user)}
  let(:category) { create(:category)}

  before do
    post.categories << category
    login_as(user)
    assign_role!(user, :manager, post)
    visit "/"
    click_link "Sublime Text 3"
    click_link "Edit Post"
  end

  context "with valid attributes:" do
    scenario "title can update" do
      title = "Edit Post - Sublime Text 3 - News City"
      expect(page).to have_title title

      fill_in "Title", with: "Sublime Text 4 beta"
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."
      expect(page).to have_content "Sublime Text 4 beta"
      expect(page).to_not have_content "Sublime Text 3"
    end

    scenario "subtitle can update" do
      fill_in "Subtitle", with: "Sublime Text 4 beta is the new release!"
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."
      expect(page).to have_content "Sublime Text 4 beta is the new release!"
      expect(page).to_not have_content "A text editor for everyone"
    end

    scenario "content can update" do
      fill_in "Content", with: "It's was my past editor for programming."
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."
      expect(page).to have_content "It's was my past editor for programming."
      expect(page).to_not have_content "It's a text editor who you can use for free in trial version, but it will complaining a text message for update paying the editor."
    end
  end

  context "when providing invalid attributes:" do
    scenario "title can't be less then 10 characters" do
      words = "a" * 9
      fill_in "Title", with: "words"
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Title is too short (minimum is 10 characters)"
    end

    scenario "title can't be more then 100 characters" do
      words = "a" * 101
      fill_in "Title", with: words
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Title is too long (maximum is 100 characters)"
      expect(page).to_not have_content words
    end

    scenario "title can't be blank" do
      fill_in "Title", with: ""
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Title can't be blank"
    end

    scenario "titles can't be the same" do
      create(:post, title: "The title must be unique")
      fill_in "Title", with: "The title must be unique"
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Title has already been taken"
    end

    scenario "subtitle can't be more then 100 characters" do
      words = "s" * 101
      fill_in "Subtitle", with: words
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Subtitle is too long (maximum is 100 characters)"
      expect(page).to_not have_content words
    end

    scenario "content can't be less then 30 characters" do
      words = "c" * 29
      fill_in "Content", with: words
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Content is too short (minimum is 30 characters)"
    end

    scenario "content can't be blank" do
      fill_in "Content", with: nil
      click_button "Update Post"

      expect(page).to have_content "Post has not been updated."
      expect(page).to have_content "Content can't be blank"
    end
  end

  context "upload a image" do
    scenario "removing attached files of images" do
      fill_in "Title", with: "Add image for posts"
      fill_in "Subtitle", with: "An image for the posts "
      attach_file "File", "#{Rails.root}/spec/fixtures/attachment.jpeg"
      fill_in "Content", with: "It will serve for show images about the posts."
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."

      within("#post .attachment") do
        expect(page).to have_css("img[src*='attachment.jpeg']")
      end

      click_link "Edit Post"

      check "post_remove_attachment"
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."
      expect(page).not_to have_css("img[src*='attachment.jpeg']")
    end

    scenario "upload a image from a remote location", slow: true do
      fill_in "Title", with: "change the image for posts with a remote location"
      fill_in "Subtitle", with: "Change the image for the posts with a remote location "
      fill_in "post_remote_attachment_url", with: "http://lorempixel.com/output/city-q-c-640-480-4.jpg"
      fill_in "Content", with: "It will serve for show images about the posts. With a remote location. like a link of an image."
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."

      within("#post .attachment") do
        expect(page).to have_css("img[src*='thumb_city-q-c-640-480-4.jpg']")
      end
    end

    context "when provide invalid attributes" do
      scenario "recuse when providing an image that exceeds 1 megabytes" do
        fill_in "Title", with: "Add image for posts"
        fill_in "Subtitle", with: "An image for the posts "
        attach_file "File", "#{Rails.root}/spec/fixtures/Ant_Nebula.jpg"
        fill_in "Content", with: "It will serve for show images about the posts."
        click_button "Update Post"

        expect(page).to have_content "Post has not been updated."
        expect(page).to have_content "file size must be less than 1 MB"
      end

      scenario "recuse when upload files that aren't image files" do
        fill_in "Title", with: "Add image for posts"
        fill_in "Subtitle", with: "An image for the posts "
        attach_file "File", "#{Rails.root}/spec/fixtures/file.txt"
        fill_in "Content", with: "It will serve for show images about the posts."
        click_button "Update Post"

        expect(page).to have_content "Post has not been updated."
        expect(page).to have_content "You are not allowed to upload \"txt\" files, allowed types: jpg, jpeg, gif, png"
      end
    end
  end

  context "with categories" do
    scenario  "when select the categories with checkboxes" do
      check "Sports"
      click_button "Update Post"

      expect(page).to have_content "Post has been updated."

      within("#post .category") do
        expect(page).to have_content "Sports"
      end
    end

    scenario "but cannot change the category without permission" do
      assign_role!(user, :editor, post)
      visit post_path(post)
      click_link "Edit Post"

      expect(page).not_to have_css('form div.post_categories')
    end
  end
end
