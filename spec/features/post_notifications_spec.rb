require 'rails_helper'

RSpec.feature "Users can receive notifications about post updates", type: :feature do
  let(:alice) { create(:user, email: "alice@example.com") }
  let(:bob) { create(:user, email: "bob@example.com") }
  let(:post) { create(:post, author: alice) }

  before do
    assign_role!(alice, :manager, post)
    assign_role!(bob, :manager, post)

    login_as(bob)
    visit post_path(post)
  end

  scenario "post authors automatically receive notifications" do
    fill_in "review_text", with: "Is it out yet?"
    click_button "Create Review"

    email = find_email!(alice.email)
    expected_subject = "[post] #{post.title}"
    expect(email.subject).to eq expected_subject

    click_first_link_in_email(email)
    expect(current_path).to eq post_path(post)
  end

  scenario "review authors are automatically subscribed to a post" do
    fill_in "review_text", with: "Is it out yet?"
    click_button "Create Review"
    click_link "Sign out"

    reset_mailer

    login_as(alice)
    visit post_path(post)
    fill_in "review_text", with: "Not yet - sorry!"
    click_button "Create Review"

    expect(page).to have_content "Review has been created."
    expect(unread_emails_for(bob.email).count).to eq 1
    expect(unread_emails_for(alice.email).count).to eq 0
  end
end
