require "rails_helper"

RSpec.feature "Users can view posts" do
  let(:user) {  create(:user) }
  let(:post) { create(:post, title: "Sublime Text 3", author: user) }

  before do
    login_as(user)
    assign_role!(user, :viewer, post)
  end

  scenario "with the posts details" do
    visit "/"
    click_link "Sublime Text 3"
    expect(page.current_url).to eq post_url(post)

    title = "Post - Sublime Text 3 - News City"
    expect(page).to have_title title
  end

  scenario "unless they do not have permission" do
    create(:post, title: "Hidden post who can not be showed for the users who not have the permissions")
    visit "/"
    expect(page).not_to have_content "Hidden post who can not be showed for the users who not have the permissions"
  end
end
