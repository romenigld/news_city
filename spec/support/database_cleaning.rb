# Configuring  DatabaseCleaner to reset data between tests.
# It’s significantly slower than using database transactions, but it will allow data to be shared between your tests and your Selenium thread safely and securely.
# use truncation for feature specs that are tagged with js: true, and you could use transaction for everything else.
RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with :truncation
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end
