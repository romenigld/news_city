module CapybaraFinders
  # This method simply takes some text, finds a list item on the page (that isn’t an action link) with the specified content, and then returns it.
  def list_item(content)
    find("ol:not(.actions) li", text: content)
  end

  def tag(content)
    find("div.tag", text: content)
  end
end

RSpec.configure do |c|
  c.include CapybaraFinders, type: :feature
end
