RSpec.configure do |config|
  # Include Factory Girl syntax to simplify calls to factories
  config.include FactoryBot::Syntax::Methods
end
