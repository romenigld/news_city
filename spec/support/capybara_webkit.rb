require 'selenium/webdriver'
require 'capybara/rspec'

RSpec.configure do
  Capybara::Webkit.configure do |config|
    config.debug = false
  end
  Capybara.javascript_driver = :webkit
end
