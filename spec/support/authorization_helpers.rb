module AuthorizationHelpers
  def assign_role!(user, role, post)
    Role.where(user: user, post: post).delete_all
    Role.create!(user: user, role: role, post: post)
  end
end

RSpec.configure do |c|
  c.include AuthorizationHelpers
end
