require "rails_helper"

RSpec.describe ReviewNotifier, type: :mailer do
  describe "created" do
    let(:post) { create(:post) }
    let(:post_owner) { create(:user) }
    let(:post) do
      create(:post, author: post_owner)
    end

    let(:reviewer) { create(:user) }
    let(:review) do
      Review.new(post: post, author: reviewer, text: "Test review")
    end

    let(:email) do
      ReviewNotifier.created(review, post_owner)
    end

    it 'sends out an email notification about a new review' do
      expect(email.to).to include post_owner.email
      title = "#{post.title} has been updated."
      expect(email.body.to_s). to include title
      expect(email.body.to_s). to include "#{reviewer.email} wrote:"
      expect(email.body.to_s). to include review.text
    end
  end
end
