require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  it "handles a missing post correctly" do
    get :show, id: "some-id-who-not-exists"

    expect(response).to redirect_to(posts_path)

    message = "The post you were looking for could not be found."
    expect(flash[:alert]).to eq message
  end

  it "handles permission errors by redirecting to a safe place" do
    allow(controller).to receive(:current_user)

    post = create(:post)
    get :show, id: post

    expect(response).to redirect_to(root_path)
    message = "You aren't allowed to do that."
    expect(flash[:alert]).to eq message
  end

  context "restrictions for users on posts who" do
    let(:user) { create(:user) }
    let(:category) { create(:category)}
    let(:posting) { Post.create(title: "State transitions", subtitle: "Can't be hacked.", content: "State transitions now they can't be hacked anymore.", author: user) }

    before :each do
      #posting.categories << category
      assign_role!(user, :editor, posting)
      sign_in user
    end

    it  "can edit a post, but not tag them" do
      put :update, { id: :posting.to_param,
                     title: "Editing post for editors!",
                     content: "The editor users, they cannot tag a post",
                     tag_names: "these are tags" },
                  post_id: posting.id

      posting.reload
      expect(posting.tags).to be_empty
    end

    it  "can edit a post, but not check the categories" do
      put :update, { id: :posting.to_param,
                     title: "Editing post for editors!",
                     content: "The editor users, they cannot check categories",
                     category_ids:["1"] },
                  post_id: posting.id

      posting.reload
      expect(posting.categories).to be_empty
    end

  end
end
