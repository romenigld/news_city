require 'rails_helper'

RSpec.describe ReviewsController, type: :controller do
  let(:user) { create(:user) }
  let(:state) { State.create!(name: "Hacked") }
  let(:posting) { Post.create(title: "State transitions", subtitle: "Can't be hacked.", content: "State transitions now they can't be hacked anymore.", author: user) }

  context "a user without permission to set state" do
    before :each do
      assign_role!(user, :editor, posting)
      sign_in user
    end

    it "cannot transition a state by passing through state_id" do
      post :create, { review: { text: "Did I hack it??",
                                                 state_id: state.id },
                                       post_id: posting.id   }
      posting.reload
      expect(posting.state).to be_nil
    end
  end

  context "a user without permission to tag a post" do
    before do
      assign_role!(user, :editor, posting)
      sign_in user
    end

    it "cannot tag a post when creating a review" do
      post :create, { review: { text: "Tag!",
                                tag_names: "one two " },
                      post_id: posting.id }
      posting.reload
      expect(posting.tags).to be_empty
    end
  end
end
