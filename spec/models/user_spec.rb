require 'rails_helper'

# RSpec.describe User, 'validation', type: :model do
#
# end

RSpec.describe User, 'association', type: :model do
  it { is_expected.to have_many(:roles) }
  it { is_expected.to have_many(:posts).with_foreign_key('author_id').dependent(:nullify) }
end

RSpec.describe User, 'column_specification', type: :model do
  it { is_expected.to have_db_column(:email).of_type(:string) }
  it { is_expected.to have_db_column(:encrypted_password).of_type(:string) }
  it { is_expected.to have_db_column(:reset_password_token).of_type(:string) }
  it { is_expected.to have_db_column(:reset_password_sent_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:remember_created_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:sign_in_count).of_type(:integer) }
  it { is_expected.to have_db_column(:current_sign_in_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:last_sign_in_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:current_sign_in_ip).of_type(:string) }
  it { is_expected.to have_db_column(:last_sign_in_ip).of_type(:string) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:admin).of_type(:boolean) }
  it { is_expected.to have_db_column(:archived_at).of_type(:datetime) }
  it { is_expected.to have_db_index(:email) }
  it { is_expected.to have_db_index(:reset_password_token) }
end
