require "rails_helper"

RSpec.describe Role, 'association', type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:post) }
end

RSpec.describe Role, 'column_specification', type: :model do
    it { is_expected.to have_db_column(:user_id).of_type(:integer) }
    it { is_expected.to have_db_column(:role).of_type(:string) }
    it { is_expected.to have_db_column(:post_id).of_type(:integer) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    it { is_expected.to have_db_index(:post_id) }
    it { is_expected.to have_db_index(:user_id) }
end
