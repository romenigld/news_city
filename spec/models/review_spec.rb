require 'rails_helper'

RSpec.describe Review, 'validation', type: :model do
  it { is_expected.to validate_presence_of(:text) }
end

RSpec.describe Review, 'association', type: :model do
  it { is_expected.to belong_to(:previous_state).class_name("State") }
  it { is_expected.to belong_to(:state) }
  it { is_expected.to belong_to(:post) }
  it { is_expected.to belong_to(:author).class_name("User") }
end

RSpec.describe Review, 'column_specification', type: :model do
  it { is_expected.to have_db_column(:text).of_type(:text).with_options(presence: true) }
  it { is_expected.to have_db_column(:post_id).of_type(:integer) }
  it { is_expected.to have_db_column(:author_id).of_type(:integer) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
  it { is_expected.to have_db_column(:state_id).of_type(:integer) }
  it { is_expected.to have_db_column(:previous_state_id).of_type(:integer) }
  it { is_expected.to have_db_index(:author_id) }
  it { is_expected.to have_db_index(:post_id) }
  it { is_expected.to have_db_index(:previous_state_id) }
end
