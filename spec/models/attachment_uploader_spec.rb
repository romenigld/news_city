require "rails_helper"

describe AttachmentUploader, type: :model do
  let(:post) { create(:post) }
  let(:uploader) { AttachmentUploader.new(post, :attachment) }

  before do
    AttachmentUploader.enable_processing = true
    File.open("#{Rails.root}/spec/fixtures/attachment.jpeg") { |f| uploader.store!(f) }
  end

  after do
    AttachmentUploader.enable_processing = false
    uploader.remove!
  end

  context 'the thumb version' do
    it "scales down a landscape image to be exactly 360 by 360 pixels" do
      expect(uploader.thumb).to have_dimensions(360, 360)
    end
  end

  context 'the small version' do
    it "scales down a landscape image to fit within 200 by 200 pixels" do
      expect(uploader.small_thumb).to be_no_larger_than(200, 200)
    end
  end

  it "has the correct format" do
    expect(uploader).to be_format('jpeg')
  end
end
