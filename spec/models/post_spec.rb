require "rails_helper"

describe Post, 'validation', type: :model do
   it { is_expected.to validate_length_of(:title).is_at_least(10) }
   it { is_expected.to validate_length_of(:title).is_at_most(100) }
   it { is_expected.to validate_presence_of(:title) }
   it { is_expected.to validate_uniqueness_of(:title) }
   it { is_expected.to validate_length_of(:subtitle).is_at_most(100) }
   it { is_expected.to validate_length_of(:content).is_at_least(30)}
   it { is_expected.to validate_presence_of(:content) }
end

RSpec.describe Post, 'association', type: :model do
  it { is_expected.to belong_to(:author).class_name("User").with_foreign_key(:author_id) }
  it { is_expected.to belong_to(:state) }
  it { is_expected.to have_many(:roles).dependent(:delete_all) }
  it { is_expected.to have_many(:categorizations).dependent(:destroy) }
  it { is_expected.to have_many(:categories).through(:categorizations) }
  it { is_expected.to have_many(:reviews).dependent(:destroy) }
  it { is_expected.to have_and_belong_to_many(:watchers).join_table("post_watchers").class_name("User") }
end

RSpec.describe Post, 'column_specification', type: :model do
    it { is_expected.to have_db_column(:title).of_type(:string).with_options(length: { minimum: 10, maximum: 100 }, presence: true, uniqueness: true) }
    it { is_expected.to have_db_column(:subtitle).of_type(:string).with_options(length: { maximum: 100 }) }
    it { is_expected.to have_db_column(:content).of_type(:text).with_options(length: { minimum: 30 }, presence: true) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    it { is_expected.to have_db_column(:author_id).of_type(:integer) }
    it { is_expected.to have_db_column(:attachment).of_type(:string) }
    it { is_expected.to have_db_column(:published_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:state_id).of_type(:integer) }
    it { is_expected.to have_db_index(:author_id) }
    it { is_expected.to have_db_index(:state_id) }

end
