require 'rails_helper'

RSpec.describe Category, 'validation', type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name).is_at_least(3) }
  it { is_expected.to validate_length_of(:name).is_at_most(30) }
  it { is_expected.to validate_uniqueness_of(:name) }
end

RSpec.describe Category, 'association', type: :model do
  it { is_expected.to have_many(:categorizations).dependent(:destroy) }
  it { is_expected.to have_many(:posts).through(:categorizations) }
end

RSpec.describe Category, 'column_specification', type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string).with_options(length: { minimum: 3, maximum: 30 }, presence: true, uniqueness: true) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
end
