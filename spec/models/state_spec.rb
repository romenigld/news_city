require 'rails_helper'

  RSpec.describe State, 'column_specification', type: :model do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:color).of_type(:string) }
  end
