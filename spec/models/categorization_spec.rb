require 'rails_helper'

RSpec.describe Categorization, 'validation', type: :model do
  #it { is_expected.to validate_presence_of(:post_id) }
  it { is_expected.to validate_presence_of(:category_id) }
end

RSpec.describe Categorization, 'association', type: :model do
  it { is_expected.to belong_to(:post) }
  it { is_expected.to belong_to(:category) }
end

RSpec.describe Categorization, 'column_specification' do
  it { is_expected.to have_db_column(:post_id).of_type(:integer).with_options(presence: true) }
  it { is_expected.to have_db_column(:category_id).of_type(:integer).with_options(presence: true) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
  it { is_expected.to have_db_index(:category_id) }
  it { is_expected.to have_db_index(:post_id) }
end
