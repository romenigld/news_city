FactoryBot.define do
  factory :review do
    text { "A review describing some changes that should be made to this post." }
  end
end
