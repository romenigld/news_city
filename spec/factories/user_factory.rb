FactoryBot.define do
  factory :user do
    #sequence(:email) { |n| "test#{n}@example.com" }
    email { Faker::Internet.email }
    password "password"

    trait :admin do
      admin true
    end
  end
end
