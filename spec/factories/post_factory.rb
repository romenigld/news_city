FactoryBot.define do
  factory :post do
    title    { Faker::Name.title }
    subtitle { Faker::Lorem.sentence }
    content  { Faker::Lorem.paragraph }

    trait :with_attachment do
      attachment { File.new("#{Rails.root}/spec/fixtures/attachment.jpeg") }
    end

    trait :with_publish_date do
      published_at { DateTime.now }
    end

    trait :in_the_future do
      published_at { 3.days.from_now }
    end

    trait :in_the_past do
      published_at { 3.days.ago }
    end

    trait :one_month_ago do
      published_at { 1.month.ago }
    end

  #   transient do
  #     categories_count 5
  #   end
  #
  #   factory :post_with_categories do
  #     after(:create) do |post, evaluator|
  #       (0..evaluator.categories_count).each do |i|
  #         post.categories << create(:category)
  #       end
  #     end
  #   end
   end
end
