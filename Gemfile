source 'https://rubygems.org'
ruby '2.4.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.8'
# Use sqlite3 as the database for Active Record
gem 'sqlite3', group: [:development, :test]
# Use PostgreSQL as the database in Production
gem 'pg', '~> 0.18.4',  group:  :production
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'execjs'
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'byebug'
  gem 'rspec-rails'
  gem 'factory_bot_rails'
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'
end

group :test do
  gem 'capybara', '~> 2.13.0'
  gem 'database_cleaner'
  gem "launchy"
  gem 'selenium-webdriver', '~> 2.53', '>= 2.53.4'
  #gem 'chromedriver-helper'
  gem "capybara-webkit"
  gem "timecop"
  gem 'email_spec'
end
group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring'
  gem 'pry-rails'
  gem 'pry-doc'
end

group :production do
  gem "rails_12factor"
  gem "puma"
end

gem 'awesome_print', '~> 1.8'
gem "slim-rails"
gem "font-awesome-rails"
gem 'bootstrap-sass', '~> 3.3.6'
gem 'simple_form'
gem "devise"
gem "pundit"
gem 'carrierwave', '~> 1.0'
gem 'file_validators'
gem 'mini_magick'
gem 'carrierwave-base64'
gem 'faker'
gem 'fog-aws'
