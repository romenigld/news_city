Rails.application.routes.draw do

  namespace :admin do
    root 'application#index'

    resources :posts, only: [:new, :create, :destroy]
    resources :categories
    resources :states, only: [:index, :new, :create] do
      member do
        get :make_default
      end
    end

    resources :users do
      collection do
        get :archived
      end

      member do
        patch :archive
        patch :unarchive
        patch :nullify_posts
        patch :nullify_roles
      end
    end
  end

  devise_for :users

  root "posts#index"

  resources :posts, only: [:index, :show, :edit, :update] do
    collection do
       get :published
       get :draft
       get :recent
       get :search
    end

    member do
       patch :publish
       patch :unpublish

       post :watch
     end
  end

  resources :categories, only: [:show]

  resources :posts, only: [] do
    resources :reviews, only: [:create]
    resources :tags, only: [] do
      member do
        delete :remove
      end
    end
  end
end
