require 'carrierwave/storage/abstract'
require 'carrierwave/storage/file'
require 'carrierwave/storage/fog'

if Rails.env.test?
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = false
    # config.asset_host = 'http://localhost:3000'
  end

  # make sure our uploader is auto-loaded
  AttachmentUploader

  # use different dirs when testing
  CarrierWave::Uploader::Base.descendants.each do |klass|
    next if klass.anonymous?
    klass.class_eval do
      def cache_dir
        "#{Rails.root}/spec/support/uploads/tmp"
      end

      def store_dir
        "#{Rails.root}/spec/support/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
      end
    end
  end
end

CarrierWave.configure do |config|
  config.asset_host = ActionController::Base.asset_host

  if Rails.env.production?
    config.root = Rails.root
    config.fog_provider = 'fog/aws'
    config.fog_credentials = {
      provider:              "AWS",
      aws_access_key_id:     ENV["S3_KEY"],
      aws_secret_access_key: ENV["S3_SECRET"],
      region:                ENV["S3_REGION"]
      # host:                  's3.example.com',
      # endpoint:              'https://s3.example.com:8080' http://news-city.s3-website.eu-west-3.amazonaws.com
    }
    config.fog_directory = ENV["S3_BUCKET"]
    config.fog_public     = false
    config.storage = :fog
    config.fog_attributes = { cache_control: "public, max-age=#{365.days.to_i}" }
  else
    config.storage = :file
  end
end
