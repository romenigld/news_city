unless User.exists?(email: "admin@newscity.com")
  User.create!(email: "admin@newscity.com",
               password: "password",
               admin: true)
end

[:manager, :editor, :viewer].each do |email|
  unless User.exists?(email: "#{email}@newscity.com")
    User.create!(email: "#{email}@newscity.com",
                 password: "password")
  end
end

["Things to do",
 "For kids",
 "Restaurants",
 "Bars",
 "Music and Nightlife",
 "Theater and Dance",
 "Museums",
 "Concerts",
 "Shopping",
 "Cine",
 "Sports and Health",
 "Architecture"].each do |name|
   unless Category.exists?(name: name)
     Category.create!(name: name)
   end
end

["Sublime Text 3", "Internet Explorer", "Safari Apple", "Mozilla Firefox", "Opera browser", "Brave browser"].each do |title|
  unless Post.exists?(title: title)
    post =  Post.create!(title: title,
                         subtitle: Faker::Lorem.sentence,
                         attachment: File.new("#{Rails.root}/spec/fixtures/attachment.jpeg"),
                         #remote_attachment_url: Faker::LoremPixel.image,
                         content: Faker::Lorem.paragraph(9),
                         author: User.first)

    category = Category.offset(rand(1..Category.count)).limit(rand(1..3))
    post.categories << category

    [:manager, :editor, :viewer].each do |role|
      if user = User.find_by_email("#{role}@newscity.com")
        user.roles.create(post: post, role: role)
      end
    end

    post.publish
  end
end

unless State.exists?
  State.create(name: "New", color: "#0066CC", default: true)
  State.create(name: "Open", color: "#008000")
  State.create(name: "Closed", color: "#990000")
  State.create(name: "Awesome", color: "#663399")
end
