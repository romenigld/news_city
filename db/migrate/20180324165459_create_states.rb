class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name
      t.string :color
    end

    add_reference :posts, :state, index: true, foreign_key: true
    add_reference :reviews, :state, foreign_key: true
  end
end
