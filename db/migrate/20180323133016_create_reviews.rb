class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.text :text
      t.references :post, index: true, foreign_key: true
      t.references :author, index: true

      t.timestamps null: false
    end

    add_foreign_key :reviews, :users, column: :author_id
  end
end
