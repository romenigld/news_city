class AddPreviousStateToReviews < ActiveRecord::Migration
  def change
    add_reference :reviews, :previous_state, index: true
    add_foreign_key :reviews, :states, column: :previous_state_id
  end
end
