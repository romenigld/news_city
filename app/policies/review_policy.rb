class ReviewPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    user.try(:admin?) || record.post.has_manager?(user) || record.post.has_editor?(user)
  end
end
