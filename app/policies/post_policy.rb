class PostPolicy < ApplicationPolicy

  class Scope < Scope
    def resolve
      # scope is the argument to policy_scope, in this case the Post model.
      # none is a convenience method provided by Active Record to automatically return no records,
      # no matter what other conditions may be added later

      if ( user.nil? || !user.has_roles? )
        return scope.published
      end

      if user.admin?
        return scope.all
      end

      scope.joins(:roles).where(roles: {user_id: user} )
    end
  end

  def index?
   user.try(:admin?)
  end

  def show?
    user.try(:admin?) || record.has_member?(user) || record.published_at?
  end

  def update?
    user.try(:admin?) || record.has_manager?(user) || record.has_editor?(user)
  end

  def destroy?
   user.try(:admin?)
  end

  def publish?
    user.try(:admin?) || record.has_manager?(user)
  end

  def unpublish?
    publish?
  end

  def published?
    update?
  end

  def draft?
    update?
  end

  def recent?
    update?
  end

  def change_state?
    publish?
  end

  def change_category?
    publish?
  end

  def tag?
    publish?
  end

  def search?
    show?
  end
end
