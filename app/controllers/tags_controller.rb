class TagsController < ApplicationController
  def remove
    @post = Post.find(params[:post_id])
    @tag = Tag.find(params[:id])
    authorize @post, :tag?

    @post.tags.destroy(@tag)
    head :ok
  end
end
