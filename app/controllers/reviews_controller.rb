class ReviewsController < ApplicationController
  before_action :set_post

  def create
    @creator = ReviewCreator.build(@post.reviews, current_user, sanitized_parameters)
    authorize @creator.review, :create?

    if @creator.save
      flash[:notice] = "Review has been created."
      redirect_to @post
    else
      flash.now[:alert] = "Review has not been created."
      @review = @creator.review
      render "posts/show"
    end
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def review_params
    params.require(:review).permit(:text, :state_id, :tag_names)
  end

  def sanitized_parameters
    whitelisted_params = review_params

    unless policy(@post).change_state?
      whitelisted_params.delete(:state_id)
    end

    unless policy(@post).tag?
      whitelisted_params.delete(:tag_names)
    end

    whitelisted_params
  end
end
