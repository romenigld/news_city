class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :publish, :unpublish, :watch]
  before_action :set_categories, only: [:edit, :update]

  def index
    @posts = policy_scope(Post).order('created_at DESC')
  end

  def show
    authorize @post, :show?
    @review = @post.reviews.build(state_id: @post.state_id)
  end

  def edit
    authorize @post, :update?
  end

  def update
    authorize @post, :update?
    @post.author = current_user

    if @post.update(sanitized_parameters)
      flash[:notice] = "Post has been updated."
      redirect_to @post
    else
      flash.now[:alert] = "Post has not been updated."
      render "edit"
    end
  end

  def publish
     authorize @post, :publish?
     @post.publish
     flash[:notice] =  'Post was successfully published.'
     redirect_to @post
  end

  def unpublish
     authorize @post, :unpublish?
     @post.unpublish
     flash[:notice] =  'Post was successfully unpublished.'
     redirect_to @post
  end

  def published
    @posts = policy_scope(Post.published)
    render action: :index
  end

  def draft
    @posts = policy_scope(Post.draft)
    render action: :index
  end

  def recent
    @posts = policy_scope(Post.recent)
    render action: :index
  end

  def search
    #authorize @post, :search?
    if params[:search].present?
      @posts = policy_scope(Post.where_title(params[:search]))
      # @posts = @posts.search(params[:search])
    else
      @posts
    end
    render action: :index
  end

  def watch
    authorize @post, :show?
    if @post.watchers.exists?(current_user.id)
      @post.watchers.destroy(current_user)
      flash[:notice] = "You are no longer watching this post."
    else
      @post.watchers << current_user
      flash[:notice] = "You are now watching this post."
    end

    redirect_to post_path(@post)
  end

  private

  def set_post
    @post = Post.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The post you were looking for could not be found."
    redirect_to posts_path
  end

  def set_categories
    @categories = Category.all.select(:id, :name)
  end

  def post_params
    params.require(:post).permit(:title,
                                 :subtitle,
                                 :content,
                                 :tag_names,
                                 :attachment,
                                 :attachment_cache,
                                 :remove_attachment,
                                 :remote_attachment_url,
                                 category_ids:[])
  end

  def sanitized_parameters
    whitelisted_params = post_params

    unless policy(@post).change_category?
      whitelisted_params.delete(category_ids:[])
    end

    unless policy(@post).tag?
      whitelisted_params.delete(:tag_names)
    end

    whitelisted_params
  end
end
