class Admin::PostsController < Admin::ApplicationController
  before_action :set_categories, only: [:new, :create]

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.author = current_user

    if @post.save
      flash[:notice] = "Post has been created."
      redirect_to @post
    else
      flash[:alert] = "Post has not been created."
      render "new"
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    flash[:notice] = "Post has been deleted."
    redirect_to posts_path
  end

  private

  def set_categories
    @categories = Category.all.select(:id, :name)
  end

  def post_params
    params.require(:post).permit(:title,
                                 :subtitle,
                                 :content,
                                 :tag_names,
                                 :attachment,
                                 :attachment_cache,
                                 :remove_attachment,
                                 :remote_attachment_url,
                                 :published_at,
                                 category_ids:[]
                                 )
  end
end
