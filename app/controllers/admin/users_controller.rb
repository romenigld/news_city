class Admin::UsersController < Admin::ApplicationController
  before_action :set_posts, only: [:new, :create, :edit, :update]
  before_action :set_user, only: [:show, :edit, :update, :archive, :unarchive, :nullify_posts, :nullify_roles]

  def index
    @users = User.excluding_archived.order(:email)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    build_roles_for(@user)

    if @user.save
      flash[:notice] = "User has been created."
      redirect_to admin_users_path
    else
      flash.now[:alert] = "User has not been created."
      render "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
    end

    User.transaction do
      @user.roles.clear
      build_roles_for(@user)

      if @user.update(user_params)
        flash[:notice] = "User has been updated."
        redirect_to admin_users_path
      else
        flash.now[:alert] = "User has not been updated."
        render "edit"
        raise ActiveRecord::Rollback
      end
    end
  end

  def archive
    if @user == current_user
      flash[:alert] = "You cannot archive yourself!"
    else
      @user.archive
      flash[:notice] = "User has been archived."
    end

    redirect_to archived_admin_users_url
  end

  def unarchive
    @user.unarchive
    flash[:notice] = "User has been unarchived."
    redirect_to admin_users_path
  end

  def archived
    @users = User.archived
    render action: :index
  end

  def nullify_posts
    if @user.archived_at?
      if @user.posts.exists?
        @user.nullify_posts
        flash[:warning] = "The user #{@user} has been nullified the posts as author."
      else
        flash[:notice] = "The user #{@user} has not posts as author to be nullified."
      end
    else
      flash[:notice] = "Can't be nullify the posts of the user #{@user}, because it isn't an archived user."
    end

    redirect_to admin_user_path(@user)
  end

  def nullify_roles
    if @user.archived_at?
      if @user.roles.exists?
        @user.nullify_roles
        flash[:warning] = "The user #{@user} has been nullified the roles in posts who was member."
      else
        flash[:notice] = "The user #{@user} has no roles on posts to be nullified."
      end
    else
      flash[:notice] = "Can't be nullify the posts of the user #{@user}, because it isn't an archived user."
    end

    redirect_to admin_user_path(@user)
  end

  private

  def set_posts
    @posts = Post.order(created_at: :desc)
  end

  def build_roles_for(user)
    role_data = params.fetch(:roles, [])
    role_data.each do |post_id, role_name|
      if role_name.present?
        user.roles.build(post_id: post_id, role: role_name)
      end
    end
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :admin)
  end
end
