class CategoriesController < ApplicationController
  skip_after_action :verify_authorized, only: [:show]
  before_action :set_category, only: [:show]

  def show
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end
end
