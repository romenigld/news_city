module PostsHelper
  def state_transition_for(review)
    if review.previous_state != review.state
      content_tag(:p) do
        value = "<strong><i class='fa fa-gear'></i> state changed</strong>"
        if review.previous_state.present?
          value += " from #{render review.previous_state}"
        end
        value += " to #{render review.state}"
        value.html_safe
      end
    end
  end

  def toggle_watching_button(post)
    text = if post.watchers.include?(current_user)
      "Unwatch"
    else
      "Watch"
    end
    link_to text, watch_post_path(post),
      class: text.parameterize, method: :post
  end
end
