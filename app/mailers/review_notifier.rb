class ReviewNotifier < ApplicationMailer
  def created(review, user)
    @review = review
    @user = user
    @post = review.post

    subject = "[post] #{@post.title}"
    mail(to: user.email, subject: subject)
  end
end
