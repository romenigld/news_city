class Review < ActiveRecord::Base
  belongs_to :previous_state, class_name: "State"
  belongs_to :state
  belongs_to :post
  belongs_to :author, class_name: "User"

  attr_accessor :tag_names

  validates :text, presence: true

  default_scope { order('created_at DESC') }
  scope :persisted, -> { where.not(id: nil) }

  before_create :set_previous_state
  after_create :set_post_state
  after_create :associate_tags_with_post
  after_create :author_watches_post

  private

    def set_previous_state
      self.previous_state = post.state
    end

    def set_post_state
      post.state = state
      post.save!
    end

    def associate_tags_with_post
      if tag_names
        tag_names.split.each do |name|
          post.tags << Tag.find_or_create_by(name: name)
        end
      end
    end

    def author_watches_post
      if author.present? && !post.watchers.include?(author)
        post.watchers << author
      end
    end
end
