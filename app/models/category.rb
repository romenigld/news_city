class Category < ActiveRecord::Base
  has_many :categorizations, dependent: :destroy
  has_many :posts, through: :categorizations

  validates :name,
                  presence: true,
                  length: { minimum: 3, maximum: 30 },
                  uniqueness: true

end
