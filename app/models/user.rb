class User < ActiveRecord::Base
  has_many :roles
  has_many :posts, :foreign_key => 'author_id',
                   :dependent => :nullify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  scope :excluding_archived, -> { where(archived_at: nil) }
  scope :archived,           -> { where("archived_at IS NOT NULL") }


  def to_s
    "#{email} (#{admin? ? "Admin" : "User"})"
  end

  def archive
    self.update(archived_at: Time.now)
  end

  def unarchive
    self.update(archived_at: nil)
  end

  def nullify_posts
    self.posts.clear
  end

  def nullify_roles
    self.roles.clear
  end

  def active_for_authentication?
    super && archived_at.nil?
  end

  def inactive_message
    archived_at.nil? ? super : :archived
  end

  def role_on(post)
    roles.find_by(post_id: post).try(:role)
  end

  def has_roles?
    if self.admin?
      true
    elsif self.roles.exists?
      true
    else
      false
    end
  end

end
