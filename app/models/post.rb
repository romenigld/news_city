class Post < ActiveRecord::Base
  before_create :assign_default_state

  belongs_to :author, class_name: "User", :foreign_key => 'author_id'
  belongs_to :state

  has_many :roles, dependent: :delete_all
  has_many :categorizations, dependent: :destroy
  has_many :categories, through: :categorizations
  has_many :reviews, dependent: :destroy
  has_and_belongs_to_many :tags, uniq: true
  has_and_belongs_to_many :watchers, join_table: "post_watchers", class_name: "User", uniq: true

  mount_base64_uploader :attachment, AttachmentUploader

  attr_accessor :tag_names

  validates :title,
              presence: true,
              length: { minimum: 10, maximum: 100 },
              uniqueness: true
  validates :subtitle,
              length: { maximum: 100 }
  validates :content,
              presence: true,
              length: { minimum: 30 }
  validates :attachment, file_size: { less_than: 1.megabytes }

  # default_scope              { order('created_at DESC') }
  scope :order_created,   -> { order('created_at DESC') }
  scope :order_published, -> { order('published_at DESC') }
  scope :published,       -> { where("published_at IS NOT NULL").order_published }
  scope :draft,           -> { where("published_at IS NULL").order_created }
  scope :recent,          -> { published.where("published_at > ?", 1.week.ago.to_date) }
  scope :where_title,     ->(term) { where("title LIKE ?", "%#{term}%") }
  scope :where_tag,       ->(term) { select('posts.id, posts.title').joins(:tags).where('tags.name == ?', "#{term}") }


  after_create :author_watches_me

  # def self.draft
  #   where("posts.published_at IS NULL")
  # end

  def published
    if self.published_at?
      " #{self.published_at.strftime('%a %d %b %Y')} at #{self.published_at.strftime('%I:%M:%S %p')}"
    else
      "not yet!"
    end
  end

  def long_title
    "#{title} - #{published_at}"
  end

  def has_member?(user)
    roles.exists?(user_id: user)
  end

  [:manager, :editor, :viewer].each do |role|
    define_method "has_#{role}?" do |user|
      roles.exists?(user_id: user, role: role)
    end
  end

  def publish
    self.update(published_at: Time.zone.now)
  end

  def unpublish
    self.update(published_at: nil)
  end

  def tag_names=(names)
    @tag_names = names
    names.split.each do |name|
      self.tags << Tag.find_or_initialize_by(name: name)
    end
  end

  private

    def assign_default_state
      self.state ||= State.default
    end

    def author_watches_me
      if author.present? && !self.watchers.include?(author)
        self.watchers << author
      end
    end
end
