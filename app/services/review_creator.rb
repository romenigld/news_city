class ReviewCreator
  attr_reader :review

  def self.build(scope, current_user, review_params)
    review = scope.build(review_params)
    review.author = current_user

    new(review)
  end

  def initialize(review)
    @review = review
  end

  def save
    if @review.save
      notify_watchers
    end
  end

  def notify_watchers
    (@review.post.watchers - [@review.author]).each do |user|
      ReviewNotifier.created(@review, user).deliver_now
    end
  end
end
